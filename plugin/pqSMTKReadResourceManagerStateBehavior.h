//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqSMTKReadResourceManagerStateBehavior_h
#define pqSMTKReadResourceManagerStateBehavior_h

#include "pqReaction.h"

#include <QObject>

class pqServer;
class pqSMTKWrapper;

/// A reaction for writing a resource manager's state to disk.
class pqReadResourceManagerStateReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqReadResourceManagerStateReaction(QAction* parent);

  static void loadResourceManagerState();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { pqReadResourceManagerStateReaction::loadResourceManagerState(); }

private:
  Q_DISABLE_COPY(pqReadResourceManagerStateReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqSMTKReadResourceManagerStateBehavior
  : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqSMTKReadResourceManagerStateBehavior* instance(QObject* parent = nullptr);
  ~pqSMTKReadResourceManagerStateBehavior() override;

protected:
  pqSMTKReadResourceManagerStateBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqSMTKReadResourceManagerStateBehavior);
};

#endif
