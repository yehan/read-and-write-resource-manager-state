//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqSMTKResourceManagerStateAutoStart.h"

#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"

#include "pqSMTKReadResourceManagerStateBehavior.h"
#include "pqSMTKWriteResourceManagerStateBehavior.h"

#include "pqApplicationCore.h"

pqSMTKResourceManagerStateAutoStart::pqSMTKResourceManagerStateAutoStart(QObject* parent)
  : Superclass(parent)
{
}

pqSMTKResourceManagerStateAutoStart::~pqSMTKResourceManagerStateAutoStart()
{
}

void pqSMTKResourceManagerStateAutoStart::startup()
{
  // We require the singleton pqSMTKBehavior so we access/create it now. If
  // smtk's smtkPQComponentsPlugin is loaded first, the two behaviors will
  // already be instantiated and the following calls will have no effect.
  // Otherwise, the accessor methods will implicitlly create the behavior's
  // singleton instance.
  auto smtkBehavior = pqSMTKBehavior::instance();

  // Access/create the singleton instances of our behaviors.
  auto readResourceManagerStateBehavior = pqSMTKReadResourceManagerStateBehavior::instance(this);
  auto writeResourceManagerStateBehavior = pqSMTKWriteResourceManagerStateBehavior::instance(this);

  // Register our behaviors with the application core.
  auto pqCore = pqApplicationCore::instance();
  if (pqCore)
  {
    pqCore->registerManager("smtk read resource manager state", readResourceManagerStateBehavior);
    pqCore->registerManager("smtk write resource manager state", writeResourceManagerStateBehavior);
  }
}

void pqSMTKResourceManagerStateAutoStart::shutdown()
{
  // Unregister our behavior from the application core.
  auto pqCore = pqApplicationCore::instance();
  if (pqCore)
  {
    pqCore->unRegisterManager("smtk read resource manager state");
    pqCore->unRegisterManager("smtk write resource manager state");
  }
}
