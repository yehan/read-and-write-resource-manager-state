//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_resource_manager_state_Registrar_h
#define __smtk_resource_manager_state_Registrar_h

#include "operators/Exports.h"

#include "smtk/operation/Manager.h"

namespace smtk
{
namespace resource_manager_state
{
class SMTKREADWRITERESOURCEMANAGERSTATE_EXPORT Registrar
{
public:
  static void registerTo(const smtk::operation::Manager::Ptr&);
  static void unregisterFrom(const smtk::operation::Manager::Ptr&);
};
}
}

#endif
